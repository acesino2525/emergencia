# emergencia
EMERGENCIA 

CASTRO, Jotshua - MU N° 00157
DIAZ, Leonel Alexander - MU N° 00154


# emergencia
EMERGENCIA 

CASTRO, Jotshua - MU N° 00157
DIAZ, Leonel Alexander - MU N° 00154


# CRUD AFILIADO

![Texto alternativo](https://gitlab.com/acesino2525/emergencia/-/raw/main/README/CRUDAfiliado.png)

__CREATE:__
Creas cargando los textfields

__READ:__
Lees desde una tabla los datos más importantes.
Puedes ver más dando editar.
FALTA: crear un pop up para visualizar todos los datos sin poner en riesgo los datos

__UPDATE:__
Desde la tabla de lectura puedes seleccionar para editar

__DELETE:__
Desde la tabla de lectura puedes seleccionar para eliminar.
FALTA: un pop up que confirme la acción
