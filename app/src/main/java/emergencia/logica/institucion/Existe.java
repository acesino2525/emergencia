package emergencia.logica.institucion;

public class Existe {
    private Boolean existe = false;
    private int index = 0;

    public Existe(Boolean existe, int index) {
        this.existe = existe;
        this.index = index;
    }

    public Boolean getExiste() {
        return existe;
    }

    public void setExiste(Boolean existe) {
        this.existe = existe;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    
    
}