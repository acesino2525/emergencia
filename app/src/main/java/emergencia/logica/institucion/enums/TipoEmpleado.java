package emergencia.logica.institucion.enums;

public enum TipoEmpleado{
    ADMINISTRATIVO,
    CHOFER,
    ENFERMERO,
    MEDICO
}
