package emergencia.logica.institucion;

import emergencia.logica.gestionAsistenciaMedicas.registroAsistencias.RegistroAsistencia;
import java.util.ArrayList;

import emergencia.logica.gestionAfiliados.Afiliado;
import emergencia.logica.gestionAfiliados.registroAfiliados.registroAfiliados;
import emergencia.logica.gestionAsistenciaMedicas.Asistencia;
import emergencia.logica.gestionEmpleados.coleccionEmpleados.RecursoHumano;
import emergencia.logica.gestionMoviles.ColeccionMoviles.MovilesColeccion;
import emergencia.logica.gestionMoviles.Movil;

public class Empresa {
    private static Empresa instance;

    // REGISTRO AFILIADOS
    private registroAfiliados registroAfiliados;
    // REGISTRO ASISTENCIAS
    private RegistroAsistencia registroAsistencias;
    // REGISTRO EMPLEADOS
    private RecursoHumano registroRecursosHumanos;
    // REGISTRO MOVILES
    private MovilesColeccion registroMoviles;

    private Empresa(){
        registroAfiliados = new registroAfiliados(new ArrayList<Afiliado>());
        registroAsistencias = new RegistroAsistencia(new ArrayList<Asistencia>());
        registroRecursosHumanos = new RecursoHumano(new ArrayList<Empleado>());
        registroMoviles = new MovilesColeccion(new ArrayList<Movil>());
    }

    public static Empresa getInstance(){
        if(instance == null){
            instance = new Empresa();
        }

        return instance;
    }

    public static void setInstance(Empresa instance) {
        Empresa.instance = instance;
    }



    public registroAfiliados getRegistroAfiliados() {
        return registroAfiliados;
    }

    public void setRegistroAfiliados(registroAfiliados registroAfiliados) {
        this.registroAfiliados = registroAfiliados;
    }

    private RegistroAsistencia getRegistroAsistencias() {
        return registroAsistencias;
    }

    private void setRegistroAsistencias(RegistroAsistencia registroAsistencias) {
        this.registroAsistencias = registroAsistencias;
    }

    private RecursoHumano getRegistroRecursosHumanos() {
        return registroRecursosHumanos;
    }

    private void setRegistroRecursosHumanos(RecursoHumano registroRecursosHumanos) {
        this.registroRecursosHumanos = registroRecursosHumanos;
    }

    private MovilesColeccion getRegistroMoviles() {
        return registroMoviles;
    }

    private void setRegistroMoviles(MovilesColeccion registroMoviles) {
        this.registroMoviles = registroMoviles;
    }

    /*
     * 
     * 
     * 
     * 
     * EJEMPLO DE USO EN APP:
     * 
     * public class App {
    public static void main(String[] args) {
        // Obtener la instancia de Empresa
        Empresa empresa = Empresa.getInstance();

        // Acceder a los métodos getters y setters
        registroAfiliados registroAfiliados = empresa.getRegistroAfiliados();
        empresa.setRegistroAfiliados(new registroAfiliados(new ArrayList<>()));
        
        RegistroAsistencia registroAsistencias = empresa.getRegistroAsistencias();
        empresa.setRegistroAsistencias(new RegistroAsistencia(new ArrayList<>()));

        RecursoHumano registroRecursosHumanos = empresa.getRegistroRecursosHumanos();
        empresa.setRegistroRecursosHumanos(new RecursoHumano(new ArrayList<>()));

        MovilesColeccion registroMoviles = empresa.getRegistroMoviles();
        empresa.setRegistroMoviles(new MovilesColeccion(new ArrayList<>()));
        
        // Realizar otras operaciones con la instancia de Empresa
        // ...
    }
}


     */

    
}
