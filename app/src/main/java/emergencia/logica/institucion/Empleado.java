package emergencia.logica.institucion;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
// import emergencia.logica.GestionEmpleados.coleccionEmpleados.RecursoHumano;
import emergencia.logica.institucion.enums.TipoEmpleado;


public class Empleado extends Persona {

    protected int sueldo;
    protected LocalDate fecha_ingreso = LocalDate.now();
    protected TipoEmpleado tipoEmpleado;

    public Empleado(String cuit, String nombre, String apellido, LocalDate fecha_nacimiento, String email,
            String telefono, Domicilio domicilio, int sueldo, LocalDate fecha_ingreso, TipoEmpleado tipoEmpleado) {
        super(cuit, nombre, apellido, fecha_nacimiento, email, telefono, domicilio);
        this.sueldo = sueldo;
        this.fecha_ingreso = fecha_ingreso;
        this.tipoEmpleado = tipoEmpleado;
    }

    

    public int getSueldo() {
        return sueldo;
    }

    public void setSueldo(int sueldo) {
        this.sueldo = sueldo;
    }

    public LocalDate getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(LocalDate fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    // METODOS PARTICULARES
    // OBTENER LA ANTIGUEDAD EN DÍAS DE UN EMPLEADO
    public long getAntiguedad(String cuit){
        
        LocalDate fechaHoy = LocalDate.now();
        return ChronoUnit.DAYS.between(fecha_ingreso, fechaHoy);
    }



    public TipoEmpleado getTipoEmpleado() {
        return tipoEmpleado;
    }



    public void setTipoEmpleado(TipoEmpleado tipoEmpleado) {
        this.tipoEmpleado = tipoEmpleado;
    }
}
