package emergencia.logica.institucion;

public class Domicilio {
    private String calle;
    private int numero;
    private int piso;
    private String departamento;
    private int cod_postal;
    private String provincia;
    private String localidad;

    public Domicilio(String calle, int numero, int piso, String departamento, int cod_postal, String provincia,
            String localidad) {

        this.calle = calle;
        this.numero = numero;
        this.piso = piso;
        this.departamento = departamento;
        this.cod_postal = cod_postal;
        this.provincia = provincia;
        this.localidad = localidad;

    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getPiso() {
        return piso;
    }

    public void setPiso(int piso) {
        this.piso = piso;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public int getCod_postal() {
        return cod_postal;
    }

    public void setCod_postal(int cod_postal) {
        this.cod_postal = cod_postal;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    @Override
    public String toString() {
        return "Domicilio{" + "calle=" + calle + ", numero=" + numero + ", piso=" + piso + ", departamento=" + departamento + ", cod_postal=" + cod_postal + ", provincia=" + provincia + ", localidad=" + localidad + '}';
    }

    
}
