package emergencia.logica.gestionPagos;
import java.time.LocalDate;

import emergencia.logica.gestionAfiliados.Afiliado;

public class BoletaPago {
    private String ticketPago;
    private String medioPago;
    private Float montoAbonado;
    private LocalDate fecha_pago = LocalDate.now();
    private Boolean eliminada = false;

    // asociaciones
    private Afiliado afiliado;

    public BoletaPago(String ticketPago, String medioPago, Float montoAbonado, LocalDate fecha_pago,
            Afiliado afiliado, Boolean eliminada) {
        this.ticketPago = ticketPago;
        this.medioPago = medioPago;
        this.montoAbonado = montoAbonado;
        this.fecha_pago = fecha_pago;
        this.afiliado = afiliado;
        this.eliminada = eliminada;
    }

    /*
     * Clases personalizadas
     */

    
    
     public String getTicketPago() {
        return ticketPago;
    }

    public void setTicketPago(String ticketPago) {
        this.ticketPago = ticketPago;
    }

    public Afiliado getAfiliado() {
        return afiliado;
    }

    public void setAfiliado(Afiliado afiliado) {
        this.afiliado = afiliado;
    }

    public String getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    public LocalDate getFecha_pago() {
        return fecha_pago;
    }

    public void setFecha_pago(LocalDate fecha_pago) {
        this.fecha_pago = fecha_pago;
    }



    public Float getMontoAbonado() {
        return montoAbonado;
    }



    public void setMontoAbonado(Float montoAbonado) {
        this.montoAbonado = montoAbonado;
    }

    public Boolean getEliminada() {
        return eliminada;
    }

    public void setEliminada(Boolean eliminada) {
        this.eliminada = eliminada;
    }

    

}
