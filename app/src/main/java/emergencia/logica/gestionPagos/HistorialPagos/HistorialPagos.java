package emergencia.logica.gestionPagos.HistorialPagos;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import emergencia.logica.exceptionPkg.ExceptionNoAccesible;
import emergencia.logica.exceptionPkg.ExceptionNoEncontrado;
import emergencia.logica.gestionPagos.BoletaPago;
import emergencia.logica.institucion.Existe;

public class HistorialPagos {
    private List<BoletaPago> coleccionPagos = new ArrayList<BoletaPago>();

    public HistorialPagos(List<BoletaPago> coleccionPagos) {
        this.coleccionPagos = coleccionPagos;
    }

    public List<BoletaPago> getColeccionPagos() {
        return coleccionPagos;
    }

    public void setColeccionPagos(List<BoletaPago> coleccionPagos) {
        this.coleccionPagos = coleccionPagos;
    }

    public class PagosException extends Exception{
        public PagosException(String msg){
            super(msg);
        }
    }


    public Existe existe(String ticketPago) throws ExceptionNoAccesible{
        
        if(coleccionPagos.isEmpty()){
            throw new ExceptionNoAccesible("La colección que buscas está vacía");
        }

        Existe existe = new Existe(false, 0);
        int i = 0;
        for(BoletaPago boleta: coleccionPagos){
            if(boleta.getTicketPago().equals(ticketPago)){
                existe.setExiste(true);
                existe.setIndex(i);
                break;
            }
            i++;
        }

        return existe;
    }

    /*
     * CRUD
     */

    /*
     * CREATE - CREAR BOLETA
     */

    public void crearBoleta(BoletaPago boleta) throws ExceptionNoAccesible{
        if(boleta == null){
            throw new ExceptionNoAccesible("El objeto que intentas añadir es nulo");
        }
        
    }

    // DELETE - ELIMINAR BOLETA
    public void eliminarBoleta(String ticketPago) throws ExceptionNoEncontrado, ExceptionNoAccesible{
        Existe existe = existe(ticketPago);

        if(!existe.getExiste()){
            throw new ExceptionNoEncontrado("La boleta que desea eliminar no existe");
        }else if(coleccionPagos.get(existe.getIndex()).getEliminada()){
            throw new ExceptionNoAccesible("El elemento ya ha sido eliminado");
        }

        coleccionPagos.get(existe.getIndex()).setEliminada(true);

    }

    // DELETE - ELIMINAR DEFINITIVAMENTE
    public void eliminarDefinitivamenteBoleta(String ticketPago) throws ExceptionNoEncontrado, ExceptionNoAccesible{
        Existe existe = existe(ticketPago);

        if(!existe.getExiste()){
            throw new ExceptionNoEncontrado("La boleta que desea eliminar no existe");
        }

        coleccionPagos.remove(existe.getIndex());

    }

    // MÉTODOS PARTICULARES

    // OBTENER LAS BOLETAS ABONADAS ASOCIADAS A DETERMINADO CUIT
    public ArrayList<BoletaPago> getBoletasAbonadas(String cuit) throws ExceptionNoEncontrado, ExceptionNoAccesible{
        Existe existe = existe(cuit);
        if(!(existe.getExiste())){
            throw new ExceptionNoEncontrado("No hay coincidencias con tu busqueda");
        }

        ArrayList<BoletaPago> boletasAfiliado = new ArrayList<BoletaPago>();
        for(BoletaPago boleta:coleccionPagos){
            if(boleta.getAfiliado().getCuit().equals(cuit)){
                boletasAfiliado.add(boleta);
            }
        }

        return boletasAfiliado;
    }

    // OBTENER ULTIMA FECHA ABONADA DE DETERMINADO CUIT
    public LocalDate getFechaUltimoPago(String cuit) throws ExceptionNoEncontrado, ExceptionNoAccesible{
        ArrayList<BoletaPago> boletasAbonadas = getBoletasAbonadas(cuit);

        return boletasAbonadas.get(boletasAbonadas.size()-1).getFecha_pago();
    }

    // OBTENER DÍAS DESDE EL ÚLTIMO PAGO
    public long getDiasDesdeUltimoPago(String cuit) throws ExceptionNoAccesible, ExceptionNoEncontrado{
        return ChronoUnit.DAYS.between(getFechaUltimoPago(cuit), LocalDate.now());
    }

    // OBTENER TODOS LOS PAGOS ENTRE DOS FECHAS
    public ArrayList<BoletaPago> getPagosEntreFechas(LocalDate desde, LocalDate hasta) throws ExceptionNoAccesible, ExceptionNoEncontrado{
        if(coleccionPagos.isEmpty()){
            throw new ExceptionNoAccesible("La coleccion a la que quieres acceder está vacía");
        }
        
        ArrayList<BoletaPago> boletas = new ArrayList<BoletaPago>();

        for(BoletaPago boleta: coleccionPagos){
            if((boleta.getFecha_pago().isEqual(desde) || boleta.getFecha_pago().isAfter(desde)) && (boleta.getFecha_pago().isEqual(hasta) || boleta.getFecha_pago().isBefore(hasta))){
                boletas.add(boleta);
            }
        }

        return boletas;
    }
    
}
