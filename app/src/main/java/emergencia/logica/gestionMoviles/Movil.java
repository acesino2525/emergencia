package emergencia.logica.gestionMoviles;

public class Movil {
    private String marca;
    private int anio;
    private String modelo;
    private String patente;
    private boolean disponible;


    public Movil(String marca, int anio, String modelo, String patente, boolean disponible) {
        this.marca = marca;
        this.anio = anio;
        this.modelo = modelo;
        this.patente = patente;
        this.disponible = disponible;
    }

    

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPatente() {
        return patente;
    }



    public void setPatente(String patente) {
        this.patente = patente;
    }



    public boolean isDisponible() {
        return disponible;
    }



    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

}
