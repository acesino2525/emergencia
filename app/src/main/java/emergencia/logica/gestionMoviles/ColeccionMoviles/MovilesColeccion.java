package emergencia.logica.gestionMoviles.ColeccionMoviles;

import java.util.ArrayList;
import java.util.List;

import emergencia.logica.exceptionPkg.ExceptionNoAccesible;
import emergencia.logica.exceptionPkg.ExceptionNoEncontrado;
import emergencia.logica.exceptionPkg.ExceptionYaExiste;
import emergencia.logica.gestionMoviles.Movil;
import emergencia.logica.institucion.Existe;

public class MovilesColeccion {
    private List<Movil> coleccionMoviles = new ArrayList<Movil>();

    public MovilesColeccion(List<Movil> coleccionMoviles) {
        this.coleccionMoviles = coleccionMoviles;
    }

    /*
     * ¿Existe ya este movil?
     */

    public Existe existe(String patente) throws ExceptionNoAccesible{
        if(coleccionMoviles.isEmpty() || coleccionMoviles == null){
            throw new ExceptionNoAccesible("Es posible que el array no se haya inicializado o se encuentre vacío");
        }
        int i = 0;
        Existe existe = new Existe(false, i);

        for(Movil movil:coleccionMoviles){
            if(movil.getPatente().equals(patente)){
                existe.setExiste(true);
                existe.setIndex(i);
            }
            i++;
        }

        return existe;
    }

    /*
     *  MÉTODOS DEL CRUD
     */

    // CREATE - CREAR UN NUEVO MOVIL
    public void agregarMovil(Movil movil) throws ExceptionNoAccesible, ExceptionYaExiste{
       Existe existe = existe(movil.getPatente());
        if(existe.getExiste()){
            throw new ExceptionYaExiste("Tal parece que este movil ya existe, por lo que no puedes crearlo. ¿Querrias editarlo en su lugar?");
        }
        // Agregamos el movil si no existe
        coleccionMoviles.add(movil);
    }

    /*
     * READ - OBTENER UN MOVIL SI COINCIDE
     */

     public Movil obtenerMovil(Movil movil) throws ExceptionNoAccesible, ExceptionNoEncontrado{
        Existe existe = existe(movil.getPatente());
        if(existe.getExiste()){
            // Retornamos el objeto en la posición dada
            return coleccionMoviles.get(existe.getIndex());
        }else{
            throw new ExceptionNoEncontrado("Tal parece no existe el movil.");
        }
        
    }

    /*
     * UPDATE - ACTUALIZAR EL MOVIL
     */

    public void actualizarMovil(Movil movil) throws ExceptionNoAccesible, ExceptionNoEncontrado{
        Existe existe = existe(movil.getPatente());
        if(!existe.getExiste()){
            throw new ExceptionNoEncontrado("Tal parece que este movil no existe.");
        }
        // Reemplazamos el movil
        coleccionMoviles.set(existe.getIndex(),movil);
    }

    /*
     * DELETE - ELIMINAR MOVIL
     */

    public void eliminarMovil(Movil movil) throws ExceptionNoAccesible, ExceptionNoEncontrado{
        Existe existe = existe(movil.getPatente());
        if(!existe.getExiste()){
            throw new ExceptionNoEncontrado("Tal parece que este movil no existe.");
        }

        coleccionMoviles.remove(existe.getIndex());
    }

    public List<Movil> getColeccionMoviles() throws ExceptionNoAccesible{
        if(coleccionMoviles == null)
        {
            throw new ExceptionNoAccesible("No existe una instancia de la coleccion");
        }
        return coleccionMoviles;
    }

    public void setColeccionMoviles(List<Movil> coleccionMoviles) {
        this.coleccionMoviles = coleccionMoviles;
    }



    // METODOS PARTICULARES
    // MOSTRAR LISTA DE MÓVILES DISPONIBLES
    public ArrayList<Movil> getMovilesDisponibles() throws ExceptionNoAccesible{
        if(coleccionMoviles == null || coleccionMoviles.isEmpty()){
            throw new ExceptionNoAccesible("La lista de moviles no ha sido definida o está vacía");
        }

        ArrayList<Movil> moviles = new ArrayList<Movil>();

        for(Movil movil:coleccionMoviles){
            if(movil.isDisponible()){
                moviles.add(movil);
            }
        }

        return moviles;
    }

    
}
