package emergencia.logica.gestionEmpleados;

import java.time.LocalDate;

import emergencia.logica.institucion.Domicilio;
import emergencia.logica.institucion.Empleado;
import emergencia.logica.institucion.enums.TipoEmpleado;

public class Medico extends Empleado {

    private String especialidad;
    private boolean disponible;

    public Medico(String cuit, String nombre, String apellido, LocalDate fecha_nacimiento, String email,
            String telefono, Domicilio domicilio, int sueldo, LocalDate fecha_ingreso, String especialidad, boolean disponible
            , TipoEmpleado tipoEmpleado) {
        super(cuit, nombre, apellido, fecha_nacimiento, email, telefono, domicilio, sueldo, fecha_ingreso, tipoEmpleado);
        this.especialidad = especialidad;
        this.disponible = disponible;
    }
    

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public boolean isDisponible() {
        return disponible;
    }



    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

}
