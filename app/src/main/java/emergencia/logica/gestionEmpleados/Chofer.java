package emergencia.logica.gestionEmpleados;

import java.time.LocalDate;

import emergencia.logica.institucion.Domicilio;
import emergencia.logica.institucion.Empleado;
import emergencia.logica.institucion.enums.TipoEmpleado;

public class Chofer extends Empleado {

    private String tipoLicencia;
    private boolean disponible;

    public Chofer(String cuit, String nombre, String apellido, LocalDate fecha_nacimiento, String email,
            String telefono, Domicilio domicilio, int sueldo, LocalDate fecha_ingreso, String tipoLicencia, boolean disponible
            , TipoEmpleado tipoEmpleado) {
        super(cuit, nombre, apellido, fecha_nacimiento, email, telefono, domicilio, sueldo, fecha_ingreso, tipoEmpleado);
        this.tipoLicencia = tipoLicencia;
        this.disponible = disponible;
    }

    public String getTipoLicencia() {
        return tipoLicencia;
    }

    public void setTipoLicencia(String tipoLicencia) {
        this.tipoLicencia = tipoLicencia;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

}
