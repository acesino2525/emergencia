package emergencia.logica.gestionEmpleados.coleccionEmpleados;

import java.util.ArrayList;
import java.util.List;

import emergencia.logica.exceptionPkg.ExceptionNoAccesible;
import emergencia.logica.exceptionPkg.ExceptionNoEncontrado;
import emergencia.logica.exceptionPkg.ExceptionYaExiste;
import emergencia.logica.institucion.Empleado;
import emergencia.logica.institucion.Existe;
import emergencia.logica.institucion.enums.TipoEmpleado;

public class RecursoHumano {
    private List<Empleado> empleadoList = new ArrayList<Empleado>();

    public RecursoHumano(ArrayList<Empleado> empleadoList) {
        this.empleadoList = empleadoList;
    }

    public List<Empleado> getEmpleadoList() {
        return empleadoList;
    }

    public void setEmpleadoList(ArrayList<Empleado> empleadoList) {
        this.empleadoList = empleadoList;
    }

    public void agregarEmpleado(Empleado empleado) {
        empleadoList.add(empleado);
    }

    public class EmpleadoException extends Exception {
        public EmpleadoException(String msg) {
            super(msg);
        }
    }

    // Comprobar si existe
    public Existe existe(String cuit) throws ExceptionNoAccesible {
        if(empleadoList == null){
            throw new ExceptionNoAccesible("La lista no está disponible");
        }
        Existe existe = new Existe(false, 0);
        int i = 0;

        for (Empleado empleado : empleadoList) {
                
            if (empleado.getCuit().equals(cuit)) {
                existe.setExiste(true);
                existe.setIndex(i);
             }
             i++;
        }

        return existe;
    }

    /*
     * CRUD
     */

    /*
     * CREATE - CREAR EMPLEADO
     */

    public void crearEmpleado(Empleado empleado) throws ExceptionNoAccesible, ExceptionYaExiste{
        Existe existe = existe(empleado.getCuit());

        if(existe.getExiste().equals(true)){
            throw new ExceptionYaExiste("Ya hay un empleado con este cuit, te recomiendo actualizarlo");
        }

        empleadoList.add(empleado);
    }

    /*
     * READ - obtener determinado empleado
     */

    public Empleado obtenerEmpleado(String cuit) throws ExceptionNoAccesible, ExceptionNoEncontrado{
        
        Existe existe = existe(cuit);

        if(existe.getExiste().equals(true)){
            return empleadoList.get(existe.getIndex());
        }
        throw new ExceptionNoEncontrado("No se han encontrado resultados");
    }

    /*
     * UPDATE - Actualizar empleado
     */

    public void actualizarEmpleado(Empleado empleado) throws ExceptionNoAccesible, ExceptionNoEncontrado{
        Existe existe = existe(empleado.getCuit());

        if(existe.getExiste().equals(true)){
            empleadoList.set(existe.getIndex(), empleado);
        }
        throw new ExceptionNoEncontrado("No se han encontrado resultados");
    }

    /*
     * DELETE - Eliminar empleado
     */

     public void eliminarEmpleado(String cuit) throws ExceptionNoAccesible, ExceptionNoEncontrado{
        
        Existe existe = existe(cuit);
        if(existe.getExiste().equals(true)){
            empleadoList.remove(existe.getIndex());
        }
        throw new ExceptionNoEncontrado("No se han encontrado resultados para este cuit");
     }


    // METODOS PARTICULARES
    // OBTENER CANTIDAD DE EMPLEADOS
     public int getCantidadEmpleados() throws ExceptionNoAccesible{
        if(empleadoList == null){
            throw new ExceptionNoAccesible("No existe una lista para contar");
        }
        return empleadoList.size();
     }

    // OBTENER LOS EMPLEADOS DISPONIBLES
    private ArrayList<Empleado> getEmpleadosDisponibles() throws ExceptionNoAccesible{
        if(empleadoList == null){
            throw new ExceptionNoAccesible("La colección no ha sido definida");
        }

        ArrayList<Empleado> empleados = new ArrayList<Empleado>();

        for(Empleado empleado:empleadoList){
            if(empleado.getTipoEmpleado() != TipoEmpleado.ADMINISTRATIVO){
                empleados.add(empleado);
            }
        }

        return empleados;
    }

    // DEVOLVER ENFERMEROS DISPONIBLES
    public ArrayList<Empleado> getEnfermerosDisponibles() throws ExceptionNoAccesible{
        ArrayList<Empleado> enfermeros = getEmpleadosDisponibles();
        int i = 0;
        for(Empleado enfermero: enfermeros){
            if(enfermero.getTipoEmpleado() != TipoEmpleado.ENFERMERO){
                enfermeros.remove(i);
            }
            i++;
        }
        return enfermeros;
    }

    // DEVOLVER MEDICOS DISPONIBLES
    public ArrayList<Empleado> getMedicosDisponibles() throws ExceptionNoAccesible{
        ArrayList<Empleado> medicos = getEmpleadosDisponibles();
        int i = 0;
        for(Empleado medico: medicos){
            if(medico.getTipoEmpleado() != TipoEmpleado.MEDICO){
                medicos.remove(i);
            }
            i++;
        }
        return medicos;
    }

    // DEVOLVER CHOFERES DISPONIBLES
    public ArrayList<Empleado> getChoferesDisponibles() throws ExceptionNoAccesible{
        ArrayList<Empleado> choferes = getEmpleadosDisponibles();
        int i = 0;
        for(Empleado chofer: choferes){
            if(chofer.getTipoEmpleado() != TipoEmpleado.CHOFER){
                choferes.remove(i);
            }
            i++;
        }
        return choferes;
    }


}
