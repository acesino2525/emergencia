package emergencia.logica.gestionEmpleados;

import java.time.LocalDate;

import emergencia.logica.institucion.Domicilio;
import emergencia.logica.institucion.Empleado;
import emergencia.logica.institucion.enums.TipoEmpleado;

public class Administrativo extends Empleado {

    public String categoria;

    public Administrativo(String cuit, String nombre, String apellido, LocalDate fecha_nacimiento, String email,
            String telefono, Domicilio domicilio, int sueldo, LocalDate fecha_ingreso, String categoria, TipoEmpleado tipoEmpleado) {
        super(cuit, nombre, apellido, fecha_nacimiento, email, telefono, domicilio, sueldo, fecha_ingreso, tipoEmpleado);
        this.categoria = categoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

}
