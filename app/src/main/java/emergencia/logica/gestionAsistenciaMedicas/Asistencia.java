package emergencia.logica.gestionAsistenciaMedicas;

import java.time.LocalDate;

import emergencia.logica.exceptionPkg.*;

import emergencia.logica.gestionAfiliados.Afiliado;
import emergencia.logica.gestionMoviles.Movil;
import emergencia.logica.institucion.Empleado;

public class Asistencia {

    private Afiliado afiliado;
    private Prioridad prioridad;
    private String descripcion;
    private LocalDate fechaAsistencia;
    private Movil ambulancia;
    private Diagnostico diagnostico;
    // asociado un conductor
    private Empleado chofer;

    // asociado un medico
    private Empleado medico;

    // asociado un enfermero
    private Empleado enfermero;

    public Asistencia(Afiliado afiliado, Prioridad prioridad, String descripcion, LocalDate fechaAsistencia,
            Movil ambulancia, Diagnostico diagnostico, Empleado chofer, Empleado medico,
            Empleado enfermero) {
        this.afiliado = afiliado;
        // this.prioridad = prioridad;
        this.descripcion = descripcion;
        this.fechaAsistencia = fechaAsistencia;
        this.ambulancia = ambulancia;
        this.diagnostico = diagnostico;
        this.chofer = chofer;
        this.medico = medico;
        this.enfermero = enfermero;
    }

    public enum Prioridad {
        ATENCION_INMEDIATA,
        EMERGENCIA,
        URGENCIA,
        PRIORITARIO,
        NO_URGENTE;
    }

    

    public Empleado getChofer() {
        return chofer;
    }

    public void setChofer(Empleado chofer) {
        this.chofer = chofer;
    }

    public Empleado getMedico() {
        return medico;
    }

    public void setMedico(Empleado medico) {
        this.medico = medico;
    }

    public Empleado getEnfermero() {
        return enfermero;
    }

    public void setEnfermero(Empleado enfermero) {
        this.enfermero = enfermero;
    }

    public Asistencia() {
    }

    public Afiliado getAfiliado() {
        return afiliado;
    }

    public void setAfiliado(Afiliado afiliado) {
        this.afiliado = afiliado;
    }

    public Prioridad getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Prioridad prioridad) {
        this.prioridad = prioridad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public LocalDate getFechaAsistencia() {
        return fechaAsistencia;
    }

    public void setFechaAsistencia(LocalDate fechaAsistencia) {
        this.fechaAsistencia = fechaAsistencia;
    }

    public Movil getAmbulancia() {
        return ambulancia;
    }

    public void setAmbulancia(Movil ambulancia) {
        this.ambulancia = ambulancia;
    }

    public Diagnostico getDiagnostico() throws DiagnosticoException {
        if (diagnostico == null) {
            throw new DiagnosticoException("No se ha asignado ningún diagnóstico.");
        }
        return diagnostico;
    }

    public void setDiagnostico(Diagnostico diagnostico) {
        this.diagnostico = diagnostico;
    }

    // cuanto tiempo paso desde la ultima asistencia (trae la cantidad de dias que
    // paso desde la ultima)
    /*
     * public int calcularDiasDesdeUltimaAsistencia() {
     * if (asistenciaList.isEmpty()) {
     * return 0; // No hay asistencias, retorna 0 días
     * }
     * 
     * Asistencia ultimaAsistencia = asistenciaList.get(asistenciaList.size() - 1);
     * LocalDate fechaUltimaAsistencia = ultimaAsistencia.getFechaAsistencia();
     * LocalDate fechaActual = LocalDate.now();
     * Period periodo = Period.between(fechaUltimaAsistencia, fechaActual);
     * int diasTranscurridos = periodo.getDays();
     * return diasTranscurridos;
     * }
     */

}
// Me marca error al querer realizar la composicion, no permite tener una clase
// extenar con el mismo nombre
/*
 * public class Diagnostico {
 * 
 * private Afiliado afiliado;
 * private Movil medico;
 * private String diagnostico;
 * private String descripcion;
 * private Asistencia asistencia;
 * 
 * public Diagnostico(Afiliado afiliado, Movil medico, String diagnostico,
 * String descripcion,
 * Asistencia asistencia) {
 * this.afiliado = afiliado;
 * this.medico = medico;
 * this.diagnostico = diagnostico;
 * this.descripcion = descripcion;
 * this.asistencia = asistencia;
 * }
 * 
 * public Diagnostico() {
 * }
 * 
 * public Afiliado getAfiliado() {
 * return afiliado;
 * }
 * 
 * public void setAfiliado(Afiliado afiliado) {
 * this.afiliado = afiliado;
 * }
 * 
 * public Movil getMedico() {
 * return medico;
 * }
 * 
 * public void setMedico(Movil medico) {
 * this.medico = medico;
 * }
 * 
 * public String getDiagnostico() {
 * return diagnostico;
 * }
 * 
 * public void setDiagnostico(String diagnostico) {
 * this.diagnostico = diagnostico;
 * }
 * 
 * public String getDescripcion() {
 * return descripcion;
 * }
 * 
 * public void setDescripcion(String descripcion) {
 * this.descripcion = descripcion;
 * }
 * 
 * public Asistencia getAsistencia() {
 * return asistencia;
 * }
 * 
 * public void setAsistencia(Asistencia asistencia) {
 * this.asistencia = asistencia;
 * }
 * 
 * }
 */