package emergencia.logica.gestionAsistenciaMedicas;

import emergencia.logica.gestionEmpleados.Medico;
import emergencia.logica.gestionAfiliados.Afiliado;

public class Diagnostico {

    private Afiliado afiliado;
    private Medico medico;
    private String diagnostico;
    private String descripcion;
    private Asistencia asistencia;

    public Diagnostico(Afiliado afiliado, Medico medico, String diagnostico, String descripcion,
            Asistencia asistencia) {
        this.afiliado = afiliado;
        this.medico = medico;
        this.diagnostico = diagnostico;
        this.descripcion = descripcion;
        this.asistencia = asistencia;
    }

    public Diagnostico() {
    }

    public Afiliado getAfiliado() {
        return afiliado;
    }

    public void setAfiliado(Afiliado afiliado) {
        this.afiliado = afiliado;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Asistencia getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(Asistencia asistencia) {
        this.asistencia = asistencia;
    }

}
