package emergencia.logica.gestionAsistenciaMedicas.registroAsistencias;

import java.util.ArrayList;
import java.util.List;

import emergencia.logica.exceptionPkg.AfiliadoException;
import emergencia.logica.gestionAsistenciaMedicas.Asistencia;

import java.time.LocalDate;

public class RegistroAsistencia {
    private List<Asistencia> asistenciaList = new ArrayList<Asistencia>();

    public RegistroAsistencia(ArrayList<Asistencia> asistenciaList) {
        this.asistenciaList = asistenciaList;
    }

    public List<Asistencia> getAsistenciaList() {
        return asistenciaList;
    }

    public void setAsistenciaList(List<Asistencia> asistenciaList) {
        this.asistenciaList = asistenciaList;
    }

    public void agregarAsistenciaMedica(Asistencia asistencia) {
        asistenciaList.add(asistencia);
    }

    // obtener asistencia generadas desde x fecha
    public List<Asistencia> getAsitenciasDesdeFecha(LocalDate desde) {
        ArrayList<Asistencia> asistenciaDesde = new ArrayList<Asistencia>();

        for (Asistencia asistencia : asistenciaList) {
            if (asistencia.getFechaAsistencia().isAfter(desde)) {
                asistenciaDesde.add(asistencia);
            }
        }

        return asistenciaDesde;
    }

    // obtener asistencias entre 2 fechas

    public List<Asistencia> getAsistenciaDesdeHasta(LocalDate desde, LocalDate hasta) {
        ArrayList<Asistencia> asistenciaFiltro = new ArrayList<Asistencia>();

        for (Asistencia asistencia : asistenciaList) {
            if (asistencia.getFechaAsistencia().isAfter(desde) && asistencia.getFechaAsistencia().isBefore(hasta)) {
                asistenciaFiltro.add(asistencia);
            }
        }

        return asistenciaFiltro;
    }

    // Cantidad de asistencias que ha recibido un afiliado

    public int calcularCantidadDeAsistencias(String cuit) throws AfiliadoException {
        int cantidadAsistencia = 0;
        for (Asistencia asistencia : asistenciaList) {
            if (asistencia.getAfiliado().getCuit().equals(cuit)) {
                cantidadAsistencia++;

                return cantidadAsistencia;
            }
        }
        throw new AfiliadoException("No se encontró ningún afiliado con el CUIT especificado");
    }

}
