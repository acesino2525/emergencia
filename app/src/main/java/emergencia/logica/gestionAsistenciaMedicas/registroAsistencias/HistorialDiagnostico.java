package emergencia.logica.gestionAsistenciaMedicas.registroAsistencias;

import java.util.ArrayList;
import java.util.List;

import emergencia.logica.exceptionPkg.AfiliadoException;
import emergencia.logica.gestionAsistenciaMedicas.Diagnostico;

public class HistorialDiagnostico {
    private List<Diagnostico> diagnosticoList = new ArrayList<Diagnostico>();

    public HistorialDiagnostico(List<Diagnostico> diagnosticoList) {
        this.diagnosticoList = diagnosticoList;
    }

    public List<Diagnostico> getDiagnosticoList() {
        return diagnosticoList;
    }

    public void setDiagnosticoList(List<Diagnostico> diagnosticoList) {
        this.diagnosticoList = diagnosticoList;
    }

    public void agregarDiagnostico(Diagnostico diagnostico) {
        diagnosticoList.add(diagnostico);
    }

    // Obtener todos los diagnosticos de un afiliado

    public ArrayList<Diagnostico> getDiagnosticoAfiliado(String cuil) throws AfiliadoException {
        ArrayList<Diagnostico> afiliadoDiag = new ArrayList<Diagnostico>();

        for (Diagnostico diagnostico : diagnosticoList) {
            if (diagnostico.getAfiliado().getCuit().equals(cuil)) {
                afiliadoDiag.add(diagnostico);
            }
            throw new AfiliadoException("No se encontró ningún afiliado con el CUIT especificado");

        }

        return afiliadoDiag;
    }

}
