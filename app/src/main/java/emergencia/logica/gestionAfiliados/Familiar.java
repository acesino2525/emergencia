package emergencia.logica.gestionAfiliados;
import java.time.LocalDate;

import emergencia.logica.institucion.Domicilio;
import emergencia.logica.institucion.Persona;

public class Familiar extends Persona {
    private LocalDate fecha_afiliacion = LocalDate.now();
    private String parentesco;
    private Afiliado afiliado;

    public Familiar(String cuit, String nombre, String apellido,
    LocalDate fecha_nacimiento, String email, String telefono, Domicilio domicilio, String parentesco, Afiliado afiliado){

        super(cuit, nombre, apellido, fecha_nacimiento, email, telefono, domicilio);
        this.parentesco = parentesco;
        this.afiliado = afiliado;

    }


    public LocalDate getFecha_afiliacion() {
        return fecha_afiliacion;
    }

    public void setFecha_afiliacion(LocalDate fecha_afiliacion) {
        this.fecha_afiliacion = fecha_afiliacion;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }


    public Afiliado getAfiliado() {
        return afiliado;
    }


    public void setAfiliado(Afiliado afiliado) {
        this.afiliado = afiliado;
    }
}
