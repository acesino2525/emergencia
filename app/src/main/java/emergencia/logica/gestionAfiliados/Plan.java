package emergencia.logica.gestionAfiliados;
// actualizamos esto, usaremos enum para
    // definir las únicas opciones de los respectivos planes
public enum Plan {
    SIMPLE(1000.00),
    COMPLETO(2000.00),
    PARCIAL(0.00);

    private double precio;

    private Plan(double precio){
        this.precio = precio;
    }

    public double getPrecio(){
        return this.precio;
    }
}