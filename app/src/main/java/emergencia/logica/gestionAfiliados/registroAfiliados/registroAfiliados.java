package emergencia.logica.gestionAfiliados.registroAfiliados;

import java.util.ArrayList;
import java.util.List;

import emergencia.logica.exceptionPkg.ExceptionNoAccesible;
import emergencia.logica.exceptionPkg.ExceptionNoEncontrado;
import emergencia.logica.exceptionPkg.ExceptionYaExiste;
import emergencia.logica.gestionAfiliados.Afiliado;
import emergencia.logica.institucion.Existe;

public class registroAfiliados {
    // Lo creamos vacío para que no devuelva null
    private List<Afiliado> afiliadosList = new ArrayList<Afiliado>();

    /*
     * Constructor
     */

    public registroAfiliados(ArrayList<Afiliado> afiliadosList) {
        this.afiliadosList = afiliadosList;
    }
    
    /*
     * Métodos propios:
     */

    // retorna un objeto con booleano y posición del objeto
    public Existe existe(String cuit) throws ExceptionNoAccesible{
        if(afiliadosList == null){
            throw new ExceptionNoAccesible("No se ha creado la lista");
        }
        Existe existe = new Existe(false, 0);
        int i = 0;
        // Recorremos en busca de coincidencias
        for(Afiliado afiliado:afiliadosList){
            if(afiliado.getCuit() == cuit){
                existe.setExiste(true);
                existe.setIndex(i);
                return existe;
            }
            i++;
        }
        // Sino, devolvemos que no se encontró nada y un index 0
        return existe;
    }

    /*
    * CRUD
    */

    /*
     * CREATE - ADD
     */

    public void addAfiliado(Afiliado afiliado) throws ExceptionNoAccesible, ExceptionYaExiste{
        Existe existe = existe(afiliado.getCuit());
        if(existe.getExiste().equals(true)){
            throw new ExceptionYaExiste("Ya existe este afiliado");
        }

        afiliadosList.add(afiliado);
    }

    

    /*
     * READ - LOOP
     */

     public Afiliado getAfiliado(String cuit) throws ExceptionNoAccesible, ExceptionNoEncontrado{
        Existe existe = existe(cuit);
        if(existe.getExiste().equals(true)){
            return afiliadosList.get(existe.getIndex());
        }else{
            throw new ExceptionNoEncontrado("El elemento que buscas no se ha encontrado");
        }
    }

    /*
     * UPDATE
     */
    public void updateAfiliado(Afiliado updatedAfiliado) throws ExceptionNoEncontrado, ExceptionNoAccesible{
        /*Existe existe = existe(updatedAfiliado.getCuit());
        if(existe.getExiste().equals(true)){
            afiliadosList.set(existe.getIndex(), updatedAfiliado);
            return;
        }else{
            throw new ExceptionNoEncontrado("No se ha encontrado el elemento");
        }*/
        int contador = 0;
        for(Afiliado a:afiliadosList){
            if(a.getCuit().equals(updatedAfiliado.getCuit())){
                afiliadosList.set(contador, updatedAfiliado);
                return;
            }
            contador++;
        }
        throw new ExceptionNoEncontrado("No se ha encontrado");
    }


    /*
     * DELETE - REMOVE
     */

    public void removeAfiliado(String cuit) throws ExceptionNoAccesible, ExceptionNoEncontrado{
        Existe existe = existe(cuit);
        if(existe.getExiste().equals(true)){
            afiliadosList.remove(existe.getIndex());
        }else{
            throw new ExceptionNoEncontrado("No encontrado");
        }
        
    }

    public List<Afiliado> getAfiliadosList() {
        return afiliadosList;
    }

    public void setAfiliadosList(ArrayList<Afiliado> afiliadosList) {
        this.afiliadosList = afiliadosList;
    }

    @Override
    public String toString() {
        return "registroAfiliados{" + "afiliadosList=" + afiliadosList + '}';
    }


    
}
