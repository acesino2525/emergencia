package emergencia.logica.gestionAfiliados;
import java.time.LocalDate;
import emergencia.logica.institucion.Domicilio;
import emergencia.logica.institucion.Persona;

public class Afiliado extends Persona{

    private LocalDate fecha_afiliacion = LocalDate.now();
    private LocalDate fechaUltimoPago = LocalDate.now();
    // asociamos el plan
    private Plan plan;

    public Afiliado(String cuit, String nombre, String apellido, 
    LocalDate fecha_nacimiento, String email, String telefono, LocalDate fecha_afiliacion, Domicilio domicilio, LocalDate fechaUltimoPago){

        super(cuit, nombre, apellido, fecha_nacimiento, email, telefono, domicilio);
        
        this.fecha_afiliacion = fecha_afiliacion;
        this.fechaUltimoPago = fechaUltimoPago;

    }

    public LocalDate getFecha_afiliacion() {
        return fecha_afiliacion;
    }

    public void setFecha_afiliacion(LocalDate fecha_afiliacion) {
        this.fecha_afiliacion = fecha_afiliacion;
    }



    public Plan getPlan() {
        return plan;
    }



    public void setPlan(Plan plan) {
        this.plan = plan;
    }




    public LocalDate getFechaUltimoPago() {
        return fechaUltimoPago;
    }




    public void setFechaUltimoPago(LocalDate fechaUltimoPago) {
        this.fechaUltimoPago = fechaUltimoPago;
    }

    @Override
    public String toString() {
        return "Afiliado{" + "fecha_afiliacion=" + fecha_afiliacion + ", fechaUltimoPago=" + fechaUltimoPago + ", plan=" + plan + '}';
    }
    
    
}

