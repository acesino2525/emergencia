package emergencia.logica.exceptionPkg;

public class EmpleadoException extends Exception {

    public EmpleadoException(String mensaje) {
        super(mensaje);
    }
}
