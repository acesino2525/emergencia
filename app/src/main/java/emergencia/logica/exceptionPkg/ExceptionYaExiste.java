package emergencia.logica.exceptionPkg;

public class ExceptionYaExiste extends Exception{
    public ExceptionYaExiste(String mensaje){
        super(mensaje);
    }
}
