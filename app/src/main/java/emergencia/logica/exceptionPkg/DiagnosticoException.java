package emergencia.logica.exceptionPkg;

public class DiagnosticoException extends Exception {

    public DiagnosticoException(String mensaje) {
        super(mensaje);
    }
}