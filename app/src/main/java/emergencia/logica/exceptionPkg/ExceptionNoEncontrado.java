package emergencia.logica.exceptionPkg;

public class ExceptionNoEncontrado extends Exception{
    public ExceptionNoEncontrado(String mensaje){
        super(mensaje);
    }
}
