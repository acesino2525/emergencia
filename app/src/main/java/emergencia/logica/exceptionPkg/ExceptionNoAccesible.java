package emergencia.logica.exceptionPkg;

public class ExceptionNoAccesible extends Exception{
    public ExceptionNoAccesible(String mensaje){
        super(mensaje);
    }
}
