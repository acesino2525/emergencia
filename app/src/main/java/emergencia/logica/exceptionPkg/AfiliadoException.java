package emergencia.logica.exceptionPkg;

public class AfiliadoException extends Exception {

    public AfiliadoException(String mensaje) {
        super(mensaje);
    }
}