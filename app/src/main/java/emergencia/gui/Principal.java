
package emergencia.gui;

import emergencia.gui.alta.*;
import emergencia.gui.controlAsistencia.NuevaAsistencia;
import emergencia.gui.pagos.TicketPago;
import emergencia.gui.ver_editar.VerAfiliadoFamiliar;
import emergencia.gui.ver_editar.VerEmpleado;
import emergencia.gui.ver_editar.VerMovil;
import emergencia.logica.institucion.Empresa;

public class Principal extends javax.swing.JFrame {
    public Empresa baseDeDatos;
    
    public Principal() {
        initComponents();
    }
    
    public Principal(Empresa baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        escritorio = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuNuevoAfiliado = new javax.swing.JMenuItem();
        jMenuNuevoMovil = new javax.swing.JMenuItem();
        jMenuNuevoFamiliar = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuNuevoChofer = new javax.swing.JMenuItem();
        jMenuNuevoMedico = new javax.swing.JMenuItem();
        jMenuNuevoEnfermero = new javax.swing.JMenuItem();
        jMenuNuevoAdmin = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuVerAfiliado = new javax.swing.JMenuItem();
        jMenuVerEmpelado = new javax.swing.JMenuItem();
        jMenuMovil = new javax.swing.JMenuItem();
        jMenuPago = new javax.swing.JMenu();
        jMenuPagar = new javax.swing.JMenuItem();
        jMenuAsistencia = new javax.swing.JMenu();
        jMenuNuevaAsistencia = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(659, 853));

        javax.swing.GroupLayout escritorioLayout = new javax.swing.GroupLayout(escritorio);
        escritorio.setLayout(escritorioLayout);
        escritorioLayout.setHorizontalGroup(
            escritorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 641, Short.MAX_VALUE)
        );
        escritorioLayout.setVerticalGroup(
            escritorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 758, Short.MAX_VALUE)
        );

        jMenu1.setText("Nuevo");

        jMenuNuevoAfiliado.setText("Nuevo Afiliado");
        jMenuNuevoAfiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuNuevoAfiliadoActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuNuevoAfiliado);

        jMenuNuevoMovil.setText("Nuevo Movil");
        jMenuNuevoMovil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuNuevoMovilActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuNuevoMovil);

        jMenuNuevoFamiliar.setText("Nuevo Familiar");
        jMenuNuevoFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuNuevoFamiliarActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuNuevoFamiliar);

        jMenu4.setText("Nuevo Empleado");

        jMenuNuevoChofer.setText("Nuevo Chofer");
        jMenuNuevoChofer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuNuevoChoferActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuNuevoChofer);

        jMenuNuevoMedico.setText("Nuevo Medico");
        jMenuNuevoMedico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuNuevoMedicoActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuNuevoMedico);

        jMenuNuevoEnfermero.setText("Nuevo Enfermero");
        jMenuNuevoEnfermero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuNuevoEnfermeroActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuNuevoEnfermero);

        jMenuNuevoAdmin.setText("Nuevo Administrativo");
        jMenuNuevoAdmin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuNuevoAdminActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuNuevoAdmin);

        jMenu1.add(jMenu4);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Ver/Editar");

        jMenuVerAfiliado.setText("Ver Afiliados");
        jMenuVerAfiliado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuVerAfiliadoActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuVerAfiliado);

        jMenuVerEmpelado.setText("Ver Empleados");
        jMenuVerEmpelado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuVerEmpeladoActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuVerEmpelado);

        jMenuMovil.setText("Ver Movil");
        jMenuMovil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuMovilActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuMovil);

        jMenuBar1.add(jMenu2);

        jMenuPago.setText("Pagos");
        jMenuPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuPagoActionPerformed(evt);
            }
        });

        jMenuPagar.setText("Pagar");
        jMenuPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuPagarActionPerformed(evt);
            }
        });
        jMenuPago.add(jMenuPagar);

        jMenuBar1.add(jMenuPago);

        jMenuAsistencia.setText("Asistencia");
        jMenuAsistencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuAsistenciaActionPerformed(evt);
            }
        });

        jMenuNuevaAsistencia.setText("Nueva Asistencia");
        jMenuNuevaAsistencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuNuevaAsistenciaActionPerformed(evt);
            }
        });
        jMenuAsistencia.add(jMenuNuevaAsistencia);

        jMenuBar1.add(jMenuAsistencia);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(escritorio)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(escritorio)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuNuevoFamiliarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuNuevoFamiliarActionPerformed

        NuevoFamiliar familiar = new NuevoFamiliar();
        familiar.setVisible(true);
        familiar.setLocationRelativeTo(null);
    }// GEN-LAST:event_jMenuNuevoFamiliarActionPerformed

    private void jMenuNuevoAfiliadoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuNuevoAfiliadoActionPerformed

        NuevoAfiliado afiliado = new NuevoAfiliado(baseDeDatos);
        afiliado.setVisible(true);
        afiliado.setLocationRelativeTo(null);
    }// GEN-LAST:event_jMenuNuevoAfiliadoActionPerformed

    private void jMenuNuevoMovilActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuNuevoMovilActionPerformed
        // TO DO
        NuevoMov movil = new NuevoMov();
        movil.setVisible(true);
        movil.setLocationRelativeTo(null);
    }// GEN-LAST:event_jMenuNuevoMovilActionPerformed

    private void jMenuVerEmpeladoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuVerEmpeladoActionPerformed
        VerEmpleado verE = new VerEmpleado();
        verE.setVisible(true);
        verE.setLocationRelativeTo(null);
    }// GEN-LAST:event_jMenuVerEmpeladoActionPerformed

    private void jMenuVerAfiliadoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuVerAfiliadoActionPerformed
        VerAfiliadoFamiliar verAfi = new VerAfiliadoFamiliar(baseDeDatos);
        verAfi.setVisible(true);
        verAfi.setLocationRelativeTo(null);
    }// GEN-LAST:event_jMenuVerAfiliadoActionPerformed

    private void jMenuAsistenciaActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuAsistenciaActionPerformed

    }// GEN-LAST:event_jMenuAsistenciaActionPerformed

    private void jMenuNuevoChoferActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuNuevoChoferActionPerformed
        NuevoChofer chofer = new NuevoChofer();
        chofer.setVisible(true);
        chofer.setLocationRelativeTo(null);
    }// GEN-LAST:event_jMenuNuevoChoferActionPerformed

    private void jMenuNuevoMedicoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuNuevoMedicoActionPerformed
        NuevoMedico medico = new NuevoMedico();
        medico.setVisible(true);
        medico.setLocationRelativeTo(null);
    }// GEN-LAST:event_jMenuNuevoMedicoActionPerformed

    private void jMenuNuevoEnfermeroActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuNuevoEnfermeroActionPerformed
        NuevoEnfermero enfermero = new NuevoEnfermero();
        enfermero.setVisible(true);
        enfermero.setLocationRelativeTo(null);
    }// GEN-LAST:event_jMenuNuevoEnfermeroActionPerformed

    private void jMenuNuevoAdminActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuNuevoAdminActionPerformed
        NuevoAdministrativo admin = new NuevoAdministrativo();
        admin.setVisible(true);
        admin.setLocationRelativeTo(null);
    }// GEN-LAST:event_jMenuNuevoAdminActionPerformed

    private void jMenuMovilActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuMovilActionPerformed
        VerMovil verMo = new VerMovil();
        verMo.setVisible(true);
        verMo.setLocationRelativeTo(null);
    }// GEN-LAST:event_jMenuMovilActionPerformed

    private void jMenuPagoActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuPagoActionPerformed

    }// GEN-LAST:event_jMenuPagoActionPerformed

    private void jMenuNuevaAsistenciaActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuNuevaAsistenciaActionPerformed
        NuevaAsistencia asis = new NuevaAsistencia();
        asis.setVisible(true);
        asis.setLocationRelativeTo(null);
    }// GEN-LAST:event_jMenuNuevaAsistenciaActionPerformed

    private void jMenuPagarActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jMenuPagarActionPerformed
        TicketPago ticket = new TicketPago();
        ticket.setVisible(true);
        ticket.setLocationRelativeTo(null);
    }// GEN-LAST:event_jMenuPagarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane escritorio;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenuAsistencia;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuMovil;
    private javax.swing.JMenuItem jMenuNuevaAsistencia;
    private javax.swing.JMenuItem jMenuNuevoAdmin;
    private javax.swing.JMenuItem jMenuNuevoAfiliado;
    private javax.swing.JMenuItem jMenuNuevoChofer;
    private javax.swing.JMenuItem jMenuNuevoEnfermero;
    private javax.swing.JMenuItem jMenuNuevoFamiliar;
    private javax.swing.JMenuItem jMenuNuevoMedico;
    private javax.swing.JMenuItem jMenuNuevoMovil;
    private javax.swing.JMenuItem jMenuPagar;
    private javax.swing.JMenu jMenuPago;
    private javax.swing.JMenuItem jMenuVerAfiliado;
    private javax.swing.JMenuItem jMenuVerEmpelado;
    // End of variables declaration//GEN-END:variables
}
