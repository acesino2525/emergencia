package emergencia.logica.gestionEmpleados.coleccionEmpleados;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import emergencia.logica.institucion.DatosPruebaTest;
import emergencia.logica.institucion.Empleado;

public class RecursoHumanoTest extends DatosPruebaTest {

    @Test
    public void testExisteEmpleado_CUITExistente() {
        try {
            String cuitExistente = "3939239";
            boolean existe = existeEmpleado(cuitExistente);
            assertTrue(existe);
        } catch (Exception e) {
            fail("Se esperaba un empleado existente, pero se lanzó una excepción");
        }
    }

    @Test
    public void testExisteEmpleado_CUITNoExistente() {
        try {
            String cuitNoExistente = "444444";
            boolean existe = existeEmpleado(cuitNoExistente);
            assertFalse(existe);
        } catch (Exception e) {
            fail("Se esperaba un empleado no existente, pero se lanzó una excepción");
        }
    }

    private boolean existeEmpleado(String cuit) throws Exception {
        try {
            for (Empleado empleado : empleadoList) {
                if (empleado.getCuit().equals(cuit)) {
                    return true;
                }
            }

            return false;
        } catch (Exception exist) {
            throw new Exception("Se produjo un error al verificar la existencia del empleado.", exist);
        }
    }

}
