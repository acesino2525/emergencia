package emergencia.logica.institucion;

// import emergencia.logica.gestionEmpleados.Chofer;
// import emergencia.logica.gestionEmpleados.Enfermero;
// import emergencia.logica.gestionEmpleados.Medico;
// import emergencia.logica.gestionAfiliados.Afiliado;
import emergencia.logica.gestionAfiliados.*;
// import emergencia.logica.gestionAsistenciaMedicas.Asistencia;
import emergencia.logica.gestionAsistenciaMedicas.*;
import emergencia.logica.gestionEmpleados.*;
import emergencia.logica.gestionMoviles.Movil;

import java.time.LocalDate;
import java.util.List;

// import org.junit.Before;

import java.util.ArrayList;

public class DatosPruebaTest {

        protected List<Empleado> empleadoList;
        // = new ArrayList<Empleado>();
        protected List<Asistencia> asistenciaList = new ArrayList<Asistencia>();
        protected List<Diagnostico> diagnosticoList = new ArrayList<Diagnostico>();
        protected List<Afiliado> afiliadoList = new ArrayList<Afiliado>();
        protected List<Asistencia> asistenciaListVacia = new ArrayList<Asistencia>();

        protected Asistencia asistencia1;
        protected Asistencia asistencia2;
        protected Asistencia asistencia3;
        protected Asistencia asistencia4;
        protected Asistencia asistencia5;

        protected Diagnostico diagnostico1;
        protected Diagnostico diagnostico2;
        protected Diagnostico diagnostico3;
        protected Diagnostico diagnostico4;
        protected Diagnostico diagnostico5;

        protected Medico med1;
        protected Medico med2;
        protected Medico med3;
        protected Medico med4;

        protected Chofer chofer1;
        protected Chofer chofer2;
        protected Chofer chofer3;

        public DatosPruebaTest() {

                empleadoList = new ArrayList<Empleado>();

                Diagnostico diagnostico1 = new Diagnostico();
                Diagnostico diagnostico2 = new Diagnostico();
                Diagnostico diagnostico3 = new Diagnostico();
                Diagnostico diagnostico4 = new Diagnostico();
                Diagnostico diagnostico5 = new Diagnostico();

                // Instancio afiliados
                Afiliado per1 = new Afiliado("12345678", "10123456781", "lionel", "messi",
                                LocalDate.now(), "email1@email.com",
                                "123456789", LocalDate.of(1990, 1, 25), new Domicilio("calle", 1, 0, "N/A", 4700,
                                                "Catamarca", "capital"),
                                LocalDate.now());

                Afiliado per2 = new Afiliado("12345679", "12123456771", "marcelo",
                                "gallardo", LocalDate.now(),
                                "email2@email.com", "123456788", LocalDate.of(1993, 9, 10),
                                new Domicilio("calle", 2, 0, "N/A", 4700, "Catamarca", "capital"),
                                LocalDate.now());

                Afiliado per3 = new Afiliado("12345671", "13123456771", "maria",
                                "Paez", LocalDate.now(),
                                "email2@email.com", "123456788", LocalDate.of(1989, 12, 20),
                                new Domicilio("calle", 2, 0, "N/A", 4700, "Catamarca", "capital"),
                                LocalDate.now());

                // Instancio medicos

                Medico med1 = new Medico("39346391", "Ricardo", "Vega", LocalDate.of(1990, 5,
                                18), "med1@email", "3832323",
                                new Domicilio("calle", 1, 0, "N/A", 4700, "Catamarca", "capital"), 200000,
                                LocalDate.now(),
                                "Cardiologo");

                Medico med2 = new Medico("3939239", "Marcela", "Perez", LocalDate.of(1991, 5,
                                10), "med1@email", "3832323",
                                new Domicilio("calle", 1, 0, "N/A", 4700, "Catamarca", "capital"), 200000,
                                LocalDate.now(),
                                "Neurologo");

                Medico med3 = new Medico("39395232", "Martin", "Forte", LocalDate.of(1975, 5,
                                10), "med1@email", "3832323",
                                new Domicilio("calle", 1, 0, "N/A", 4700, "Catamarca", "capital"), 200000,
                                LocalDate.of(2023, 5, 10),
                                "Nutricionista");

                // Instancio chofer

                Chofer chofer1 = new Chofer("51523292", "Florentino", "Diaz", LocalDate.of(1999, 10, 9), "chofer@email",
                                "9228282",
                                new Domicilio("calle", 1, 0, "N/A", 4700, "Catamarca", "capital"), 123240,
                                LocalDate.now(),
                                "A1");

                Chofer chofer2 = new Chofer("515232921", "Florentino", "Diaz", LocalDate.of(1999, 10, 9),
                                "chofer@email",
                                "9228282",
                                new Domicilio("calle", 1, 0, "N/A", 4700, "Catamarca", "capital"), 123240,
                                LocalDate.now(),
                                "A1");

                Chofer chofer3 = new Chofer("3515232921", "Florentino", "Diaz", LocalDate.of(1999, 10, 9),
                                "chofer@email",
                                "9228282", new Domicilio("calle", 1, 0, "N/A", 4700, "Catamarca", "capital"), 123240,
                                LocalDate.now(),
                                "A1");

                // Instancio enfermero

                Enfermero enfermero1 = new Enfermero("11932252", "Carla", "Campos", LocalDate.of(1999, 11, 9),
                                "enfermero@email",
                                "12451232", new Domicilio("calle", 1, 0, "N/A", 4700, "Catamarca", "capital"), 150000,
                                LocalDate.now(),
                                "auxiliar");

                Enfermero enfermero2 = new Enfermero("21932252", "Carla", "Campos", LocalDate.of(1999, 11, 9),
                                "enfermero@email",
                                "12451232", new Domicilio("calle", 1, 0, "N/A", 4700, "Catamarca", "capital"), 150000,
                                LocalDate.now(),
                                "auxiliar");

                Enfermero enfermero3 = new Enfermero("31932252", "Carla", "Campos", LocalDate.of(1999, 11, 9),
                                "enfermero@email",
                                "12451232", new Domicilio("calle", 1, 0, "N/A", 4700, "Catamarca", "capital"), 150000,
                                LocalDate.now(),
                                "auxiliar");

                // Instancio Movil

                Movil ambulancia1 = new Movil("Peugeot", 2019, "208", null, chofer1, med1, enfermero1);

                Movil ambulancia2 = new Movil("Renault", 2020, "Clio", null, chofer2, med2, enfermero2);

                Movil ambulancia3 = new Movil("Fiat", 2021, "toro", null, chofer3, med3, enfermero3);

                // Intancio Asistencias

                Asistencia asistencia1 = new Asistencia(per1, Asistencia.Prioridad.ATENCION_INMEDIATA, "Llamo por esto",
                                LocalDate.of(2023, 1, 29), ambulancia1, diagnostico1);

                Asistencia asistencia2 = new Asistencia(per2, Asistencia.Prioridad.URGENCIA, "Llamo por esto",
                                LocalDate.of(2023, 5, 30), ambulancia2, diagnostico2);

                Asistencia asistencia3 = new Asistencia(per3, Asistencia.Prioridad.EMERGENCIA, "Llamo por esto",
                                LocalDate.of(2021, 5, 10),
                                ambulancia3, diagnostico3);

                Asistencia asistencia4 = new Asistencia(per1, Asistencia.Prioridad.NO_URGENTE, "Llamo por esto",
                                LocalDate.of(2022, 11, 10),
                                ambulancia3, diagnostico4);

                Asistencia asistencia5 = new Asistencia(per2, Asistencia.Prioridad.PRIORITARIO, "Llamo por esto",
                                LocalDate.of(2023, 5, 15),
                                ambulancia1, diagnostico5);

                // Diagnosticos

                diagnostico1.setAfiliado(per1);
                diagnostico1.setMedico(med1);
                diagnostico1.setDiagnostico("Padece esto");
                diagnostico1.setDescripcion("Llamo por esto");

                diagnostico2.setAfiliado(per2);
                diagnostico2.setMedico(med2);
                diagnostico2.setDiagnostico("Padece esto");
                diagnostico2.setDescripcion("Llamo por esto");

                diagnostico3.setAfiliado(per3);
                diagnostico3.setMedico(med3);
                diagnostico3.setDiagnostico("padece esto");
                diagnostico3.setDescripcion("Llamo por esto");

                diagnostico4.setAfiliado(per1);
                diagnostico4.setMedico(med3);
                diagnostico4.setDiagnostico("padece esto");
                diagnostico4.setDescripcion("Llamo por esto");

                diagnostico5.setAfiliado(per2);
                diagnostico5.setMedico(med1);
                diagnostico5.setDiagnostico("padece esto");
                diagnostico5.setDescripcion("Llamo por esto");

                // Add a arraylist recursohumano

                empleadoList.add(med1);
                empleadoList.add(med2);
                empleadoList.add(med3);

                empleadoList.add(chofer1);
                empleadoList.add(chofer2);
                empleadoList.add(chofer3);

                empleadoList.add(enfermero1);
                empleadoList.add(enfermero2);
                empleadoList.add(enfermero3);

                // Add a arraylist asistencia

                asistenciaList.add(asistencia1);
                asistenciaList.add(asistencia2);
                asistenciaList.add(asistencia3);
                asistenciaList.add(asistencia4);
                asistenciaList.add(asistencia5);

                // Add a arraylist diagnostico

                diagnosticoList.add(diagnostico1);
                diagnosticoList.add(diagnostico2);
                diagnosticoList.add(diagnostico3);
                diagnosticoList.add(diagnostico4);
                diagnosticoList.add(diagnostico5);

                // Add a arraylist afiliado

                afiliadoList.add(per1);
                afiliadoList.add(per2);
                afiliadoList.add(per3);

        }

        public Asistencia getAsistencia1Test() {
                return asistencia1;
        }

        public Asistencia getAsistencia2Test() {
                return asistencia2;
        }

        public Asistencia getAsistencia5Test() {
                return asistencia5;
        }

        public Diagnostico getDiagnosticoTest() {
                return diagnostico1;
        }

        public Medico getMedico1Test() {
                return med1;
        }

        public Medico getMedico2Test() {
                return med2;
        }

        public Medico getMedico3Test() {
                return med3;
        }

        public Chofer getChofer1Test() {
                return chofer1;
        }

        public Chofer getChofer2Test() {
                return chofer2;
        }

        public Chofer getChofer3Test() {
                return chofer3;
        }

        public List<Afiliado> getAfiliadoListTest() {
                return afiliadoList;
        }

        public List<Empleado> getEmpleadoListTest() {
                return empleadoList;
        }

        public List<Asistencia> getAsistenciaListTest() {
                return asistenciaList;
        }

        public List<Diagnostico> getDiagnosticoListTest() {
                return diagnosticoList;
        }

        public List<Asistencia> getAsistenciaVaciaListTest() {
                return asistenciaListVacia;
        }

}