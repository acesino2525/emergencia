package emergencia.logica.institucion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.Period;

import org.junit.Test;

import emergencia.logica.exceptionPkg.*;

public class EmpleadoTest extends DatosPruebaTest {

    // List<Empleado> empleadoList = getEmpleadoListTest()

    @Test
    public void testCalcularEdadEmpleado() {
        try {
            // Empleado existente en la lista de empleados
            int edadEmpleado1 = calcularEdadEmpleado("39346391");
            assertEquals(33, edadEmpleado1);

            // Empleado existente en la lista de empleados
            int edadEmpleado2 = calcularEdadEmpleado("444444");
            assertEquals(30, edadEmpleado2);

            // Empleado no encontrado en la lista de empleados
            fail("Se esperaba una excepción EmpleadoException");
        } catch (EmpleadoException e) {
            assertEquals("No se encontró ningún empleado con el CUIT especificado: 444444", e.getMessage());
        }
    }

    private int calcularEdadEmpleado(String cuit) throws EmpleadoException {
        for (Empleado empleado : empleadoList) {
            if (empleado.getCuit().equals(cuit)) {
                LocalDate fechaNacimiento = empleado.getFecha_nacimiento();
                LocalDate fechaActual = LocalDate.now();
                Period periodo = Period.between(fechaNacimiento, fechaActual);
                return periodo.getYears();
            }
        }
        throw new EmpleadoException("No se encontró ningún empleado con el CUIT especificado");
    }

    @Test
    public void testCalcularAntiguedadEnDias_CUITExistente() {
        try {
            String cuitExistente = "39395232";
            int antiguedad = calcularAntiguedadEnDias(cuitExistente);
            assertEquals(11, antiguedad);

            // Verificar que la antigüedad sea la esperada
            // ...
        } catch (EmpleadoException e) {
            fail("Se esperaba un empleado existente, pero se lanzó una excepción EmpleadoException");
        }
    }

    @Test
    public void testCalcularAntiguedadEnDias_CUITNoExistente() {
        try {
            String cuitNoExistente = "444444";
            int antiguedad = calcularAntiguedadEnDias(cuitNoExistente);
            assertEquals(20, antiguedad);
            fail("Se esperaba una excepción EmpleadoException");
        } catch (EmpleadoException e) {
            assertEquals("No se encontró ningún empleado con el CUIT especificado: 444444", e.getMessage());
        }
    }

    public int calcularAntiguedadEnDias(String cuit) throws EmpleadoException {
        for (Empleado empleado : empleadoList) {
            if (empleado.getCuit().equals(cuit)) {
                LocalDate fechaIngreso = empleado.getFecha_ingreso();
                LocalDate fechaActual = LocalDate.now();
                Period antiguedad = Period.between(fechaIngreso, fechaActual);
                return antiguedad.getDays();
            }
        }
        throw new EmpleadoException("No se encontró ningún empleado con el CUIT especificado: " + cuit);
    }

}
