package emergencia.logica.gestionAsistenciaMedicas.coleccionAsistencias;

import java.util.List;
import java.util.ArrayList;
import org.junit.Test;
import org.junit.Assert;
import emergencia.logica.institucion.DatosPruebaTest;
import emergencia.logica.exceptionPkg.AfiliadoException;
import emergencia.logica.gestionAsistenciaMedicas.Diagnostico;

public class HistorialDiagnosticoTest extends DatosPruebaTest {

    @Test
    public void getDiagnosticoAfiliadoTest_existeCuitHistorial() throws AfiliadoException {
        String cuitExiste = "101234";
        List<Diagnostico> historialAfiliado = new ArrayList<Diagnostico>();
        historialAfiliado = getDiagnosticoAfiliado(cuitExiste);
        Assert.assertEquals(3, historialAfiliado.size());
    }

    @Test
    public void getDiagnosticoAfiliadoTest_NoExisteCuitHistorial() throws AfiliadoException {
        String cuitNoExiste = "1312";
        List<Diagnostico> historialAfiliadoVacio = new ArrayList<Diagnostico>();
        historialAfiliadoVacio = getDiagnosticoAfiliado(cuitNoExiste);
        Assert.assertTrue(historialAfiliadoVacio.isEmpty());
    }

    public List<Diagnostico> getDiagnosticoAfiliado(String cuit) throws AfiliadoException {
        List<Diagnostico> afiliadoDiag = new ArrayList<Diagnostico>();

        for (Diagnostico diagnostico : diagnosticoList) {
            if (diagnostico.getAfiliado().getCuit().equals(cuit)) {
                afiliadoDiag.add(diagnostico);
            }

        }
        // if (afiliadoDiag.isEmpty()) {
        // throw new AfiliadoException("No hay ningún diagnóstico asociado al CUIT
        // ingresado");
        // }

        return afiliadoDiag;
    }

}
