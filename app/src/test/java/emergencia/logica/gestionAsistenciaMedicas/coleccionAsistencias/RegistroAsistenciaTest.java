package emergencia.logica.gestionAsistenciaMedicas.coleccionAsistencias;

import org.junit.Test;
import org.junit.Assert;
import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;
import org.junit.Before;

import emergencia.logica.exceptionPkg.AfiliadoException;
import emergencia.logica.gestionAsistenciaMedicas.Asistencia;
import emergencia.logica.institucion.DatosPruebaTest;

public class RegistroAsistenciaTest extends DatosPruebaTest {

    private List<Asistencia> asistenciaList;

    @Before
    public void setUp() {
        asistenciaList = getAsistenciaListTest();
    }

    /// Obtener la cantidad de asistencias que recibio un afiliado

    @Test
    public void testCalcularCantidadDeAsistencias_AfiliadoTiene() throws AfiliadoException {
        // Cuit perteneciente a un afiliado existente per1
        String cuit = "101234";
        // Guardo el return del metodo, en una variable int
        int cantidadDeAsistencias = calcularCantidadDeAsistencias(cuit);
        // Hay 3 instancias de Asistencia en el que se encuentra per1
        System.out.println(cantidadDeAsistencias);
        // Assert.assertEquals(3, cantidadDeAsistencias);
    }

    @Test(expected = AfiliadoException.class)
    public void testCalcularCantidadDeAsistencias_AfiliadoNoTiene() throws AfiliadoException {
        String cuit = "1312";
        // Intentar calcular la cantidad de asistencias para un afiliado no existente
        int cantidadDeAsistencia0;
        cantidadDeAsistencia0 = calcularCantidadDeAsistencias(cuit);
    }

    // El metodo que se testeo:
    public int calcularCantidadDeAsistencias(String cuit) throws AfiliadoException {
        int cantidadAsistencia = 0;
        for (Asistencia asistencia : asistenciaList) {
            if (asistencia.getAfiliado().getCuit().equals(cuit)) {
                cantidadAsistencia++;

                return cantidadAsistencia;
            }

        }
        throw new AfiliadoException("No se encontró ningún afiliado con el CUIT especificado: " + cuit);
    }

    // --------------------------------------------------------
    // obtener asistencia generadas desde x fecha

    @Test
    public void getAsistenciaDesdeFechaTest() {
        // fecha desde cuando quiero obtener las asistencias
        LocalDate desde = LocalDate.of(2022, 1, 1);
        // arraylist para almacenar las asistencias recibida
        List<Asistencia> asistenciaDesde = new ArrayList<Asistencia>();
        // llamo al metodo, el return se almacena en el arraylist de arriba
        asistenciaDesde = getAsistenciasDesdeFecha(desde);
        // comparo el tamaño del arraylist, Sé que deberia obtener 4 objetos en mi
        // arraylist
        Assert.assertEquals(4, asistenciaDesde.size());

    }

    // @Test
    // public void getAsistenciaDesdeFecha_sinAsistencias() {
    // // Le paso la fecha actual, sabiendo que no hay asistencia generada hoy
    // LocalDate fecha = LocalDate.now();
    // List<Asistencia> asistenciaVacia = new ArrayList<Asistencia>();
    // asistenciaVacia = getAsistenciasDesdeFecha(fecha);
    // // espero un arraylist vacio
    // Assert.assertTrue(asistenciaVacia.isEmpty());
    // }

    // El metodo que se testeo:
    public List<Asistencia> getAsistenciasDesdeFecha(LocalDate desde) {
        List<Asistencia> asistenciaDesde = new ArrayList<Asistencia>();

        for (Asistencia asistencia : asistenciaList) {
            if (asistencia.getFechaAsistencia().isAfter(desde)) {
                asistenciaDesde.add(asistencia);
            }
        }

        return asistenciaDesde;
    }

    // ---------------------------------------------------
    // obtener asistencias entre 2 fechas

    @Test
    public void getAsistenciaDesdeHasta() {
        // Instancio un arraylist donde almacenare el resultado
        List<Asistencia> desdeHasta = new ArrayList<Asistencia>();
        // Fechas que paso como parametros
        LocalDate desde = LocalDate.of(2021, 1, 1);
        LocalDate hasta = LocalDate.of(2023, 2, 1);
        // Llamo al metodo, return se almacena en el arraylist previo
        desdeHasta = getAsistenciaDesdeHasta(desde, hasta);
        // Comparo el tamaño, Sé que deberia recibir 3 objetos en mi arraylist
        Assert.assertEquals(3, desdeHasta.size());
    }

    @Test
    public void getAsistenciaDesdeHasta_vacio() {
        // Instancio un arraylist donde almacenare el resultado
        List<Asistencia> desdeHastaVacio = new ArrayList<Asistencia>();
        // Fechas que paso como parametros
        LocalDate desde = LocalDate.of(2023, 5, 25);
        LocalDate hasta = LocalDate.now();
        desdeHastaVacio = getAsistenciaDesdeHasta(desde, hasta);
        // espero un arraylist vacio
        Assert.assertTrue(desdeHastaVacio.isEmpty());
    }

    public List<Asistencia> getAsistenciaDesdeHasta(LocalDate desde, LocalDate hasta) {
        List<Asistencia> asistenciaFiltro = new ArrayList<Asistencia>();

        for (Asistencia asistencia : asistenciaList) {
            if (asistencia.getFechaAsistencia().isAfter(desde) && asistencia.getFechaAsistencia().isBefore(hasta)) {
                asistenciaFiltro.add(asistencia);
            }
        }

        return asistenciaFiltro;
    }

}
