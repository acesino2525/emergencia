package emergencia.logica.gestionAsistenciaMedicas;

import java.time.LocalDate;
import java.time.Period;

import org.junit.Assert;
import org.junit.Test;

import emergencia.logica.institucion.DatosPruebaTest;

public class AsistenciaTest extends DatosPruebaTest {

    @Test
    public void calcularDiasDesdeUltimaAsistenciaTest() {

        Asistencia ultimaAsistencia = asistenciaList.get(asistenciaList.size() - 1);
        LocalDate fechaUltimaAsistencia = ultimaAsistencia.getFechaAsistencia();
        LocalDate fechaActual = LocalDate.now();
        Period periodo = Period.between(fechaUltimaAsistencia, fechaActual);
        int diasTranscurridos = periodo.getDays();
        Assert.assertEquals(10, diasTranscurridos);
    }

    public int calcularDiasDesdeUltimaAsistencia() {
        if (asistenciaList.isEmpty()) {
            return 0; // No hay asistencias, retorna 0 días
        }

        Asistencia ultimaAsistencia = asistenciaList.get(asistenciaList.size() - 1);
        LocalDate fechaUltimaAsistencia = ultimaAsistencia.getFechaAsistencia();
        LocalDate fechaActual = LocalDate.now();
        Period periodo = Period.between(fechaUltimaAsistencia, fechaActual);
        int diasTranscurridos = periodo.getDays();
        return diasTranscurridos;
    }

}
