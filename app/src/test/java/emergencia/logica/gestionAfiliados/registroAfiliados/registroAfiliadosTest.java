package emergencia.logica.gestionAfiliados.registroAfiliados;

import static org.junit.Assert.assertArrayEquals;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;

import emergencia.logica.gestionAfiliados.Afiliado;
import emergencia.logica.gestionAfiliados.Familiar;
import emergencia.logica.gestionAfiliados.registroAfiliados.registroAfiliados;
import emergencia.logica.institucion.Domicilio;

public class registroAfiliadosTest {
    @Test
    public void testGetAfiliadosList() {

    }

    @Test
    public void testSetAfiliadosList() {

    }

    @Test
    public void testToString() {
        Afiliado afiliado = new Afiliado("dni", "cuit", "nombre", "apellido", LocalDate.now(), "email", "telefono", LocalDate.now(), new Domicilio(null, 0, 0, null, 0, null, null), LocalDate.now());
        ArrayList<Afiliado> afiliados = new ArrayList<>();
        afiliados.add(afiliado);
        registroAfiliados afiliadosColeccion = new registroAfiliados(afiliados);

        Afiliado[] expectedArray = afiliados.toArray(new Afiliado[0]);
        Afiliado[] actualArray = afiliadosColeccion.getAfiliadosList().toArray(new Afiliado[0]);

        assertArrayEquals(expectedArray, actualArray);
    }
}
