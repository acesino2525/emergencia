package emergencia.logica.gestionAfiliados;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;

import emergencia.logica.gestionAfiliados.Afiliado;
import emergencia.logica.gestionAfiliados.Familiar;
import emergencia.logica.institucion.Domicilio;

public class AfiliadoTest {
    @Test
    public void testGetDomicilio() {

    }

    @Test
    public void testGetFamiliares() {

    }

    @Test
    public void testGetFecha_afiliacion() {

    }

    @Test
    public void testGetPlan() {

    }

    @Test
    public void testSetDomicilio() {

    }

    @Test
    public void testSetFamiliares() {
        // creamos una lista vacía
        ArrayList<Familiar> list = new ArrayList<>();
        // guardamos constructores para su posterior uso:
        Familiar p1 = new Familiar("11111111", "20111111111", "Nombre", "Apellido", LocalDate.now(), "email@email.com", "3834444444", new Domicilio("calle", 1, 0, "N/A", 4700, "Catamarca", "capital"), "Primo", new Afiliado("dni", "cuit", "nombre", "apellido", LocalDate.now(), "email", "telefono", LocalDate.now(), new Domicilio(null, 0, 0, null, 0, null, null), LocalDate.now()));
        Familiar p2 = new Familiar("11111112", "20111111121", "Nombre", "Apellido", LocalDate.now(), "email@email.com", "3834444444", new Domicilio("calle", 1, 0, "N/A", 4700, "Catamarca", "capital"), "Primo", new Afiliado("dni", "cuit", "nombre", "apellido", LocalDate.now(), "email", "telefono", LocalDate.now(), new Domicilio(null, 0, 0, null, 0, null, null), LocalDate.now()));
        
        // agregamos elementos
        list.add(p1);
        list.add(p2);
        // TESTS
        // Controlamos el tamaño del array
        assertEquals(2, list.size());

        // Controlamos que lo creado exista en el array
        assertTrue(list.contains(p1));
        assertTrue(list.contains(p2));

    }

    @Test
    public void testSetFecha_afiliacion() {

    }

    @Test
    public void testSetPlan() {

    }
}
