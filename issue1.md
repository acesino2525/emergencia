Clase Abstracta Persona:

    Atributos:
    ID
    DNI
    Nombre
    Apellido
    Fecha de nacimiento
    Dirección
    Correo electrónico
    Teléfono
    Fecha de afiliación

    Métodos:
    Obtener información de afiliado
    Actualizar información de afiliado

Clase Afiliado extends Persona:

    extends from Persona

❓❓❓❓❓
¿Un familiar puede ser asociado al mismo tiempo?
¿No tendría doble cobertura?
A resolver...
Clase Familia extends Persona:

    extends from Persona
    dniAsociado - Atributo extra

    Método:
    Obtener Información del Asociado


Clase Plan de afiliación:

    Atributos:
    ID
    DNI
    Nombre
    Descripción
    Precio
    Beneficios

    Métodos:
    Obtener información del plan
    Actualizar información del plan

Clase Pago:

    Atributos:
    ID
    Fecha de pago
    Monto
    Método de pago

    Métodos:
    Registrar pago
    Obtener información de pago

Clase Renovación:

    Atributos:
    ID
    Fecha de renovación
    Plan de afiliación
    Afiliado

    Métodos:
    Registrar renovación
    Obtener información de renovación